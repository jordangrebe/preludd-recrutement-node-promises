import express from 'express';

class App {
    public app: express.Application;

    constructor() {
        this.app = express();

        this.config();
        this.listen();
    }

    private config(): void {
        this.app.use(express.urlencoded({extended: true}));
        this.app.use(express.json());
    }

    private listen():void {
        // Default port set to 3000
        let port = process.env.PORT || 3000;

        this.app.listen(port, async () => {
            console.log("Server paused!");
            await this.sleep(5000);
            console.log("Server unpaused!")
            
        }).on('error', (err: any) => {
            console.log('ERROR::', err.code);
            if(err.code === 'EADDRINUSE') {
                console.log(`Port ${port} est déjà utilisé`, 1);
            } else {
                console.log(err, 1);
            }
        });
    }

    private sleep(seconds: number) {
        return new Promise(resolve => setTimeout(resolve, seconds));
    }
}

export default new App().app;
