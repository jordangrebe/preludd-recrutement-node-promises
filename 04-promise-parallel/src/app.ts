import express from 'express';

class App {
    public app: express.Application;

    constructor() {
        this.app = express();

        this.config();
        this.listen();
    }

    private config(): void {
        this.app.use(express.urlencoded({extended: true}));
        this.app.use(express.json());
    }

    private listen():void {
        // Default port set to 3000
        let port = process.env.PORT || 3000;

        this.app.listen(port, async () => {
            console.log("Server started!");

            await this.parallel([ Promise.resolve(12), Promise.resolve(13) ]).then(result => console.log(result)); // Returns functions
            
        }).on('error', (err: any) => {
            console.log('ERROR::', err.code);
            if(err.code === 'EADDRINUSE') {
                console.log(`Port ${port} est déjà utilisé`, 1);
            } else {
                console.log(err, 1);
            }
        });
    }

    private parallel(functions: Array<any>) {
        return new Promise(async (resolve, reject) => {
            let results: Array<any> = [];

            for(let i in functions) {
                let item = await functions[i];

                console.log('item', typeof item)
                results.push(item)
            }

            resolve(results)
        }).catch((err) => {
            return {error: true, payloads:err}
        })
    }
}

export default new App().app;
