"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
class App {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.listen();
    }
    config() {
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use(express_1.default.json());
    }
    listen() {
        // Default port set to 3000
        let port = process.env.PORT || 3000;
        this.app.listen(port, () => __awaiter(this, void 0, void 0, function* () {
            console.log("Server started!");
            yield this.first([() => Promise.resolve(12), () => Promise.resolve(13)]).then(result => console.log(result)); // Returns functions
            yield this.first([Promise.resolve(12), Promise.resolve(13)]).then(result => console.log(result)); // Returns functions result
        })).on('error', (err) => {
            console.log('ERROR::', err.code);
            if (err.code === 'EADDRINUSE') {
                console.log(`Port ${port} est déjà utilisé`, 1);
            }
            else {
                console.log(err, 1);
            }
        });
    }
    first(functions) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let results = [];
            for (let i in functions) {
                let item = functions[i];
                results.push(yield item);
            }
            resolve(results);
        })).catch((err) => {
            console.log(err);
            return { error: true, payloads: err };
        });
    }
}
exports.default = new App().app;
