import express from 'express';

class App {
    public app: express.Application;

    constructor() {
        this.app = express();

        this.config();
        this.listen();
    }

    private config(): void {
        this.app.use(express.urlencoded({extended: true}));
        this.app.use(express.json());
    }

    private listen():void {
        // Default port set to 3000
        let port = process.env.PORT || 3000;

        this.app.listen(port, async () => {
            console.log("Server started!");
            const theOne = new Promise((resolve, reject) => {
                let x: boolean = false;

                if(x) {
                    resolve('welcome Jordan');
                } else {
                    reject('Error detected')
                }
            }).catch((err) => {
                return {failed: true, err};
            });

            await this.sequential([ () => Promise.resolve(12), theOne, () => Promise.resolve(13) ]).then(result => console.log(result)); // Returns functions
            
        }).on('error', (err: any) => {
            console.log('ERROR::', err.code);
            if(err.code === 'EADDRINUSE') {
                console.log(`Port ${port} est déjà utilisé`, 1);
            } else {
                console.log(err, 1);
            }
        });
    }

    private sequential(functions: Array<any>) {
        return new Promise(async (resolve, reject) => {
            let results: Array<any> = [];

            for(let i in functions) {
                let item = await functions[i];

                if(item.failed) {
                    reject(item)
                } else {
                    results.push(item)
                }
            }

            resolve(results)
        }).catch((err) => {
            return {error: true, payloads:err}
        })
    }
}

export default new App().app;
